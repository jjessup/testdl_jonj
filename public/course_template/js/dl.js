var AwePage = {
	enrollmentId: '',
	readUrl: '',
	writeUrl: ''
},
	API = window !== window.top ? window.parent.API : null,
	DLutil = function () {

	//determine if the file is index.html or not. If index.html, modify links
	var path = window.location.pathname,
		pathEnd = path.substring(path.length - 14),
		root = pathEnd === 'index_lms.html' || pathEnd.substring(pathEnd.length - 1, pathEnd.length) === '/' ? 'course/' : '',
		revRoot = root === 'course/' ? '' : '../',
		lmsAPIUrl = revRoot + 'js/lmsapi.js',
//		lmsAPIUrl = '/js/LMSAPI.js',
		popovers = $('[rel="popover"]'),
		toc = $('#toc'),
		dlmain = $('#dl-main'),
		fblink = $('#fblink'),
		alertTemplate = '<div class="row"><div class="span6 offset3 alert alert-block alert-{{alerttype}}">{{alertmessage}}<a class="close" data-dismiss="alert" href="#">&times;</a></div></div>';
	//PowerSource specific variables
	var isFramed = window !== window.top,
		winp = isFramed ? window.parent: null,
		//cookie retrieved values
		cookiepath = path.match(/\/dl\/courses\/\d+\//),
		cks = {
			dlfb: isFramed ? winp.AwePage.feedbackUrl : $.cookie('dlfb'),
			dlceid: isFramed ? winp.AwePage.enrollmentId : $.cookie('dlceid'),
			dlrurl: isFramed ? winp.AwePage.readUrl : $.cookie('dlrurl'),
			dlwurl: isFramed ? winp.AwePage.writeUrl : $.cookie('dlwurl')
		},
		initAwePage = function () {
			AwePage.enrollmentId = cks.dlceid;
			AwePage.readUrl = cks.dlrurl;
			AwePage.writeUrl = cks.dlwurl;
		},
		initAPI = function () {
			if (AwePage.enrollmentId && AwePage.readUrl && AwePage.writeUrl) {
				API = new LMSAPI();
			}
		};
	//general DL functions
	var notify = function (alerttype, alertmessage) {
		dlmain.prepend(alertTemplate.replace(/\{\{alerttype\}\}/g, alerttype).replace(/\{\{alertmessage\}\}/g, alertmessage));
	};
	//PowerSource specific functions
	var setSessionCookie = function (cookietype) {
			$.cookie(cookietype, cks[cookietype], {path: cookiepath});
		},
		targetOnTop = function (els) {
			els.filter('[href!="#"]').attr('target', '_top');
		},
		resumeCourse = function () {
			var val = API.LMSGetValue('cmi.suspend_data'),
				resumeLink = '',
				resumeText = '';
			if (val) {
				resumeLink = dlmain.find('a[href*="' + val + '"]');
				if (resumeLink.length !== 0) {
					resumeText = resumeLink.prev().text();
					notify('info', 'Click <i class="icon-file"></i> <a href="' + resumeLink.attr('href') + '">' + resumeText + '</a> to resume the course or choose another page below.');
				}
			}
		};
	//if there is a parent frame and there is a value, set each of the cookies
	if (winp) {
		if (cks.dlceid) {
			setSessionCookie('dlceid');
			if (cks.dlfb) {
				setSessionCookie('dlfb');
			}
			if (cks.dlrurl) {
				setSessionCookie('dlrurl');
			}
			if (cks.dlwurl) {
				setSessionCookie('dlwurl');
			}
		}
	}

	//set the feedback link if there is a cookie value for the course
	if (cks.dlfb) {
		fblink.attr('href', cks.dlfb);
	} else {
		fblink.on('click', function () {
			notify('error', 'We\'re sorry, but the survey link is not currently available.');
		});
	}
	//initialize the AwePage variables
	if (cks.dlceid) {
		initAwePage();
	}
	//create the resources modal window
	var initResourcesModal = function () {
		var resModal = $('#resourcesModal');
		resModal.load(root + 'resources.html', function () {
			resModal.find('a').each(function () {
				var self = $(this),
					href = self.attr('href');
				if (href) {
					if (href.substring(0,4) === 'res/') {
						self.attr('href', root + href);
					}
				}
			});
		});
		$(this).off('click', initResourcesModal);
	};
	//load navigation
	$('#toc-menu').load(root + 'toc.html', function () {
		var pageName = $('h1').text(),
			allTocAs = $('#toc-menu').find('a'),
			tocLi = allTocAs.filter('a:contains("'+ pageName +'")'),
			allTocAsLength = allTocAs.length,
			curnavLi = -1,
			nextText = '',
			prevText = '',
			nextHref = '',
			prevHref = '',
			prevBtn = $('#nav-prev'),
			nextBtn = $('#nav-nxt'),
			nextA = null,
			prevA = null;
		if (allTocAs.length > 0) {
			$('#toc-toggle-btn, #toc-toggle').removeClass('hidden');
		}
		if (tocLi) {
			tocLi.parent('li').addClass('active');
		}
		//if there is a root value, page is outside the course folder; modify links
		if (root) {
			var TocLinks = $('#toc-menu').find('a');
			//targetOnTop(TocLinks);
			TocLinks.each(function () {
				var self = $(this);
				if (self.html() !== '#' && !self.hasClass('dropdown-toggle')) {
					self.attr('href', 'course/' + self.attr('href'));
				}
			});
		}
		//find the current navigation index
		allTocAs.each(function (i) {
			if ($(this).attr('href') === tocLi.attr('href')) {
				curnavLi = i;
			}
		});
		switch (curnavLi) {
			case -1:
				nextA = $(allTocAs[0]);
				nextHref = nextA.attr('href');
				nextText = nextA.text();
				break;
			case 0:
				nextA = $(allTocAs[1]);
				nextHref = nextA.attr('href');
				nextText = nextA.text();
				break;
			case allTocAsLength - 1:
				prevA = $(allTocAs[curnavLi - 1]);
				prevHref = prevA.attr('href');
				prevText = prevA.text();
				break;
			default:
				prevA = $(allTocAs[curnavLi - 1]);
				nextA = $(allTocAs[curnavLi + 1]);
				nextHref = nextA.attr('href');
				nextText = nextA.text();
				prevHref = prevA.attr('href');
				prevText = prevA.text();
		}
		if (!prevHref) {
			prevBtn.hide();
		} else {
			prevBtn.attr('href', prevHref);
			prevBtn.attr('rel', 'tooltip');
			prevBtn.attr('data-original-title', prevText);
		}
		if (!nextHref) {
			nextBtn.hide();
		} else {
			nextBtn.attr('href', nextHref);
			nextBtn.attr('rel', 'tooltip');
			nextBtn.attr('data-original-title', nextText);
		}
		//targetOnTop(nextBtn);
		//targetOnTop(prevBtn);
	});
	

	//trigger popovers
	popovers.popover({trigger: 'manual', placement: 'bottom'});
	popovers.on('click', function () {
		var self = $(this);
		self.popover('toggle');
	});
	//trigger tooltips
	dlmain.tooltip({
		selector: "a[rel=tooltip]"
	});
	//attachments modal init
	$('#resourceLink').on('click', initResourcesModal);

	//if there is a root (this is the index page) and a frame on top
	if (root) {
		if (winp) {
			//change all the links to target the top rather than stay in the frame
			//targetOnTop($('#dl-main a'));
		}
		if (API) {
			resumeCourse();
		}
	}
	
	//handle clicking exit
	$('#exitlink').on('click', function () {
		var sendCurPage = function () {
			if (API) {
				API.LMSSetValue("cmi.suspend_data", 'course/' + path.split('course/')[1]);
				API.LMSCommit();
				notify('success', 'Your location has been saved. You can close the window to exit the course.');
			} else {
				$.cookie('dlsuspend', path, {path: cookiepath});
			}
		};
		if (!API) {
			$.ajax({
				url: lmsAPIUrl,
				dataType: 'script',
				success: function () {
					initAPI();
					sendCurPage();
				}
			});
		} else {
			sendCurPage();
		}
	});
	//load if LMS API is needed
	if (!API && ($('.quiz-area[data-final="true"]').length > 0 || root)) {
		$.ajax({
			url: lmsAPIUrl,
			dataType: 'script',
			success: function () {
				initAPI();
				if (root) {
					resumeCourse();
				}
			},
			error: function (j, t, e) {
				console.log('No API is available.');
			}
		});
	}
	//load if quiz-area is on page
	if ($('.quiz-area').length > 0) {
		$.ajax({
			url: '../js/quizzes.js',
			dataType: 'script'
		});
	}
	return {
		notify: notify,
		cookies: cks,
		resumeCourse: resumeCourse,
		API: API
	};
}; //end anon function on doc ready

var dl = new DLutil();