var LMSAPI = function () {
	var updated = false;
	var _scormValues = new Object();
	function doPersist () {
		if (updated) {
			var _toBeUpdated=new Object();
			_toBeUpdated=_scormValues;
			_scormValues=new Object();
			var params=new Object;
			params["courseEnrollment.id"]=AwePage.enrollmentId;
			params.scormValues=_toBeUpdated;
			$.post(AwePage.writeUrl, params, function (data) {
				var json=eval("("+data+")");
				if (json && json.result && json.result == "success") {
					
				} else {
					
				}
			});
			updated=false
		}
	}
	function persistWorker () {
		console.log("persistWorker");
		doPersist();
		setTimeout(persistWorker,10000)
	}
	return {
		LMSInitialize: function (i) {
			persistWorker();
			return"true"
		},
		LMSFinish: function (s) {
			doPersist();
			return"true"
		},
		LMSGetValue: function (s) {
			var value="";
			var params=new Object;
			params["courseEnrollment.id"] = AwePage.enrollmentId;
			if (s == "cmi.core.lesson_status") {
				params.scormKey = s;
			} else {
				if (s=="cmi.suspend_data") {
					params.scormKey = s;
				} else {
					if (s=="cmi.core.score.raw") {
						params.scormKey = s;
					} else {
						if (s=="cmi.core.score.max") {
							params.scormKey = s;
						} else {
							return "";
						}
					}
				}
			}
			$.ajaxSetup({
				async:false
			});
			$.post(AwePage.readUrl, params, function (data) {
				var json=eval("("+data+")");
				if (json&&json.result&&json.result=="success"&&json.scormValue) {
					value=json.scormValue;
				}
			});
			$.ajaxSetup({
				async: true
			});
			return value;
		},
		LMSSetValue: function (s,v) {
			if (s=="cmi.core.lesson_status") {
				if (_scormValues["'"+s+"'"] != v) {
					updated = true;
					_scormValues["'"+s+"'"] = v;
					}
			} else {
				if (s=="cmi.suspend_data") {
					if (_scormValues["'"+s+"'"] != v) {
						updated = true;
						_scormValues["'"+s+"'"] = v;
					}
				} else {
					if (s=="cmi.core.score.raw") {
						if (_scormValues["'"+s+"'"] != v) {
							updated = true;
							_scormValues["'"+s+"'"] = v;
						}
					} else {
						if (s=="cmi.core.score.max") {
							if (_scormValues["'"+s+"'"] != v) {
								updated = true;
								_scormValues["'"+s+"'"] = v;
							}
						}
					}
				}
			}
			return"true";
		},
		LMSCommit:function(s){doPersist();
			return "true";
		},
		LMSGetLastError:function () {return 0
		},
		LMSGetErrorString:function (s) {
			return "No Error";
		},
		LMSGetDiagnostic:function () {
			return "";
		}
	}
};


/* Old version preserving for code cleanliness
var LMSAPI = function () {
	var enrollmentId = '',
		readUrl = '',
		writeUrl = '',
		params = {},
		config = function (prop, val) {
			if (prop === 'enrollmentId' || 'readUrl' || 'writeUrl') {
				this[prop] = val;
			}
		},
		reset_params = function () {
			params = {};
		},
		LMSGetValue = function (val) {
			if (val === 'cmi.core.lesson_status' || 'cmi.suspend_data' || 'cmi.core.score.raw' || 'cmi.core.score.max') {
				var rurl = this.readUrl,
					ceid = this.enrollmentId;
				if (rurl && ceid) {
					var param = {
						"courseEnrollment.id": ceid,
						scormKey: val
					},
						value = '';
					$.ajaxSetup({
						async: false
					});
					$.post(rurl, param, function (data) {
						var json = eval("(" + data + ")");
						if (json && json.result && json.result === "success" && json.scormValue) {
							value = json.scormValue;
						}
					});
					$.ajaxSetup({
						async: true
					});
					return value;
				} else {
					return '';
				}
			}
		},
		LMSSetValue = function (prop, val) {
			if (prop === 'cmi.core.lesson_status' || 'cmi.core.score.raw' || 'cmi.core.score.max' || 'cmi.suspend_data') {
				params["'" + prop + "'"] = val;
			}
		},
		LMSCommit = function () {
			var wurl = this.writeUrl,
				ceid = this.enrollmentId;
			if (wurl && ceid) {
				var param = {
					"courseEnrollment.id": ceid,
					scormValues: params
				};
				$.post(wurl, param, function (data) {
					var json = eval("(" + data + ")");
				});
			} else {
				return '';
			}
			reset_params();
		};
	return {
		LMSGetValue: LMSGetValue,
		LMSSetValue: LMSSetValue,
		LMSCommit: LMSCommit,
		config: config
	};
};

if (dl && dl.cookies) {
	var API = new LMSAPI();
	API.enrollmentId = dl.cookies.dlceid;
	API.readUrl = dl.cookies.dlrurl;
	API.writeUrl = dl.cookies.dlwurl;
}
*/
